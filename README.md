# Консольное приложение для создания задач, просмотра списка задач и детального просмотра информации по конкретной задаче
## СТЕК:
    * linux
    * python
    * click
    * sqlalchemy
    * postgresql
1. Создать БД:
```angular2html
sudo -u postgres psql
CREATE DATABASE solo;
```
при желании можно создать пользователя с правами:
```angular2html
CREATE USER test_user WITH password 'qwerty';
GRANT ALL ON DATABASE test_database TO test_user;
```
2. В терминале создайте папку командой ```mkdir render```
3. Перейдите в папку командо ```cd render```
4. Введите команду для клонирования репозитория ```git clone https://gitlab.com/Suvorov-progects/render-farm.git```
5. Установить все необходимые зависимости командой ```pip install -r requirements.txt```

6. Инициализируйте Alembic командой ```alembic init -t async migrations```
   
7. Для проведения миграций необходимо:
- в файле ```alembic.ini``` указать путь к БД, напрмер ```sqlalchemy.url = postgresql+psycopg2://postgres:postgres@localhost:5432/solo```
- в папке ```migration``` в файле ```env.py``` указать путь к своим моделям, например ```from database.models import *``` и изменить 
```
from database.connected import Base
target_metadata = Base.metadata
```
8. Провести первую миграцию командой ```alembic revision --autogenerate -m "init"```
9. Применить миграцию командой ```alembic revision --autogenerate -m "init"```
10. Запустите приложение командой ```python main.py```
