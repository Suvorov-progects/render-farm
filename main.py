import click
import random
from apscheduler.schedulers.background import BackgroundScheduler

from database.connected import sess
from database.models import *


def messanger(name):
    while True:
        click.echo("----   ----   ----    ----")
        comand_m = input(
            'Введите команду (3 - создание задачи, '
            '4 - отображение всех ваших задач, '
            '5 - отображение истории конкретной задачи): ')
        if comand_m == '3':
            new_task = input(('Введите наименование задачи: '))
            task(new_task, name)

        elif comand_m == '4':
            all_task(name)

        elif comand_m == '5':
            new_task = input(('Введите наименование задачи: '))
            history_task(name, new_task)
        elif comand_m == '6':
            click.echo("До свидания! Всего хорошего!")
            break
        else:
            click.echo("Введена неверная команда, повторите!")


@click.command()
def hello():
    click.echo('Добро пожаловать в консольное приложение. Оно позволяет: \n'
               '1. Создавать запись пользователя\n'
               '2. Доступ к функционалу исключительно через аутентификацию \n'
               '3. Создавать задачи \n'
               '4. Просматривать созданные задачи \n'
               '5. Просматривать детальную информацию по конкретной задаче')
    click.echo("--------------------------------------------------------------")
    click.echo("Следуйте инструкциям и у вас все получится)")
    comand = input('Введите команду (1 - регистрация, 2 - авторизация): ')
    if comand == '1':
        name = input(('Введите ваше имя: '))
        psd = input(('Введите ваш пароль: '))
        registry(name, psd)
    elif comand == '2':
        name = input(('Введите ваше имя: '))
        psd = input(('Введите ваш пароль: '))
        authorization(name, psd)
    else:
        click.echo(f"Вы ввели не верную команду, попробуйте снова!")


def registry(name, psd):
    client = sess.query(Client).filter(Client.name == name).first()
    if client:
        click.echo(f"Такой пользователь уже существует, пожалуйста авторизируйтесь!")
        hello()
    else:
        cl = Client(name=name, psd=psd)
        sess.add(cl)
        sess.commit()
        click.echo("Регистрация прошла успешно!")
        click.echo("Что дальше?")
        while True:
            click.echo("создание новой задачи - 3")
            click.echo("отображение списка созданных задач - 4")
            click.echo("отображение истории смены статусов задачи - 5")
            click.echo("выход - 6")
            comand = input(
                'Введите команду (3 - создание задачи, '
                '4 - отображение всех ваших задач, '
                '5 - отображение истории конкретной задачи): ')
            if comand == '3':
                new_task = input(('Введите наименование задачи: '))
                task(new_task, cl.id)

            elif comand == '4':
                all_task(cl.id)

            elif comand == '5':
                new_task = input(('Введите наименование задачи: '))
                history_task(cl.id, new_task)
            elif comand == '6':
                click.echo("До свидания! Всего хорошего!")
                break
            else:
                click.echo("Введена неверная команда, повторите!")


def authorization(name, psd):
    client = sess.query(Client).filter(Client.name == name, Client.psd == psd).first()
    if client:
        click.echo("Добро пожаловать! Авторизация прошла успешно!")
        click.echo("Что дальше?")
        while True:
            click.echo("создание новой задачи - 3")
            click.echo("отображение списка созданных задач - 4")
            click.echo("отображение истории смены статусов задачи - 5")
            click.echo("выход - 6")
            comand_a = input(
                'Введите команду (3 - создание задачи, '
                '4 - отображение всех ваших задач, '
                '5 - отображение истории конкретной задачи): ')
            if comand_a == '3':
                new_task = input(('Введите наименование задачи: '))
                task(new_task, client.id)

            elif comand_a == '4':
                all_task(client.id)

            elif comand_a == '5':
                new_task = input(('Введите наименование задачи: '))
                history_task(client.id, new_task)
            elif comand_a == '6':
                click.echo("До свидания! Всего хорошего!")
                break
            else:
                click.echo("Введена неверная команда, повторите!")
    else:
        click.echo(f"Введены неверные данные, повторите попытку или пройдите регистрацию")
        click.echo(f"регистрация - введите команду python registry.py --name=Ваше имя --psd=Ваш пароль")
        name = input(('Введите вашу имя: '))
        psd = input(('Введите ваш пароль: '))
        authorization(name, psd)


def task(task, name):
    ts = Tasks(task=task, status='RENDERING', creator=name)
    sess.add(ts)
    sess.commit()
    minutes = random.randint(1, 6)
    # создаем задачу на закрытие созданной задачи
    sched.add_job(lambda: close_task(ts.id), 'interval', minutes=minutes)
    sched.start()
    click.echo('Задача создана')
    messanger(name)


def close_task(task_id):
    date_time = datetime.now()
    sess.query(Tasks).filter(Tasks.id == task_id).update({Tasks.modified: date_time, Tasks.status: 'COMPLETE'})
    sess.commit()


def all_task(name):
    tasks = sess.query(Tasks).filter(Tasks.creator == name).all()
    for i in tasks:
        click.echo(f'задача - {i.task}, статус - {i.status}')
    messanger(name)


def history_task(name, task):
    working_task = sess.query(Tasks).filter(Tasks.task == task, Tasks.creator == name).first()
    click.echo(f'RENDERING- {working_task.created}')
    click.echo(f'COMPLETE- {working_task.modified}')
    messanger(name)


if __name__ == '__main__':
    sched = BackgroundScheduler(daemon=True)
    hello()
