from datetime import datetime
from pytz import timezone

from sqlalchemy import Column, Integer, String, DateTime
from .connected import Base


UTC = timezone('UTC')


def time_now():
    return datetime.now(UTC)


class Client(Base):
    __tablename__ = "client"

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String)
    psd = Column(String)

    __mapper_args__ = {"eager_defaults": True}

    def __repr__(self):
            return (
                f"<{self.__class__.__name__}("
                f"id={self.id}, "
                f"number={self.name}, "
                f")>"
            )


class Tasks(Base):
    __tablename__ = "tasks"

    id = Column(Integer, primary_key=True, autoincrement=True)
    task = Column(String)
    creator = Column(Integer)
    created = Column(DateTime(timezone=True), default=datetime.utcnow)
    modified = Column(DateTime(timezone=True), nullable=False, default=time_now)
    status = Column(String, unique=False, default='RENDERING')

    __mapper_args__ = {"eager_defaults": True}

    def __repr__(self):
        return (f"<{self.__class__.__name__}, ("
                f"id={self.id}, "
                f"task={self.task}, "
                f"status={self.status})>"
                )
